import sys

import requests

from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

from met2influxdb import __version__
from met2influxdb.argparse import setup_parser


MET_TO = "https://aa015h6buqvih86i1.api.met.no/weatherapi/locationforecast/2.0/complete"


def air_quality(location):
    ret = requests.get(f"https://meteo.francetvinfo.fr/api/atmo/air-quality/{location}")
    if ret.status_code != 200:
        return None
    return ret.json()["score"]


def weather(url, lat, lon):
    ret = requests.get(
        url,
        params={"lat": lat, "lon": lon},
        headers={"User-Agent": f"met2influxdb v{__version__}"},
    )
    if ret.status_code != 200:
        return None

    data = ret.json()

    return {
        "time": data["properties"]["timeseries"][0]["time"],
        **data["properties"]["timeseries"][0]["data"]["instant"]["details"],
    }


def influxdb(url, token, org, bucket, data):
    client = InfluxDBClient(url=url, token=token, org=org)
    write_api = client.write_api(write_options=SYNCHRONOUS)

    point = Point("weather")
    for k, v in data.items():
        point.field(k, v)
    write_api.write(bucket=bucket, record=[point])
    client.close()


def main():
    # Parse command line
    parser = setup_parser()
    options = parser.parse_args()

    data = weather(MET_TO, options.LAT, options.LON)
    data["air_quality"] = air_quality(options.location)

    print(f"time     : {data['time']}")
    print(f"temp     : {data['air_temperature']}")
    print(f"hum      : {data['relative_humidity']}")
    print(f"wind     : {data['wind_speed']}")
    print(f"wind dir : {data['wind_from_direction']}")
    print(f"UV       : {data['ultraviolet_index_clear_sky']}")
    print(f"cloud    : {data['cloud_area_fraction']}")
    print(f"pressure : {data['air_pressure_at_sea_level']}")
    print(f"air qual : {data['air_quality']}")

    if options.influxdb_url:
        influxdb(
            options.influxdb_url,
            options.influxdb_token,
            options.influxdb_org,
            options.influxdb_bucket,
            {
                "temperature": data["air_temperature"],
                "humidity": data["relative_humidity"],
                "wind": data["wind_speed"],
                "wind_direction": data["wind_from_direction"],
                "uv": data["ultraviolet_index_clear_sky"],
                "cloud": data["cloud_area_fraction"],
                "pressure": data["air_pressure_at_sea_level"],
                "air_quality": data["air_quality"],
            },
        )
    return 0


if __name__ == "__main__":
    sys.exit(main())
