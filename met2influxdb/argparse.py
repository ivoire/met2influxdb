import argparse

from met2influxdb import __version__


##########
# Setups #
##########
def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="met2influxdb", description="met2influxdb")

    parser.add_argument(
        "--version", action="version", version=f"%(prog)s, {__version__}"
    )

    group = parser.add_argument_group("influxdb")
    group.add_argument("--influxdb-url")
    group.add_argument("--influxdb-token")
    group.add_argument("--influxdb-org")
    group.add_argument("--influxdb-bucket")

    parser.add_argument("--location")
    parser.add_argument("LAT", help="Latitude")
    parser.add_argument("LON", help="Longitude")

    return parser
